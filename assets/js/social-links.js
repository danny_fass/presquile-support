﻿$(document).ready(function () {
	function ucwords(str,force){
	  str=force ? str.toLowerCase() : str;  
	  return str.replace(/(\b)([a-zA-Z])/g,
	           function(firstLetter){
	              return   firstLetter.toUpperCase();
	           });
	}
	/*
	function autocase ( text ) {
	    return text.replace(/(&)?([a-z])([a-z]{2,})(;)?/ig,function ( all, prefix, letter, word, suffix ) {
	        if (prefix && suffix) {
	            return all;
	        }
	
	        return letter.toUpperCase() + word.toLowerCase();
	    });
	}
	*/
    $('#bookmark-module-linklist a').click(function () {
        var service = $(this).attr('id').replace('bookmark-module-linklist-', '');
        var serviceUrl = '';

        var url = "http://www.presquilewine.com" + window.location.pathname;
        var title = document.title;
        console.log("title: " + title);
        var pageHeader = ucwords( $('h1').text(), true );
        pageHeader = pageHeader.replace("Presqu'Ile","Presqu'ile");
        var ssummary = "Presqu'ile Winery is located in the Santa Maria Valley on California's Central Coast. Created by two transverse mountain ranges on the Pacific Coast, Santa Maria Valley provides a unique west-to-east flow of cool marine breezes ideal for producing exceptional Pinot Noir, Chardonnay and Sauvignon Blanc."
        url=encodeURIComponent(url);
        title=encodeURIComponent(title);
        ssummary=encodeURIComponent(ssummary);

        switch (service) {
            case 'email':
                serviceUrl = "mailto:?subject=www.presquilewine.com%20|%20" + pageHeader + "&body=" + url; 
                break;
            case 'fb': serviceUrl = "http://www.facebook.com/sharer.php?u=" + url + "&amp;t=" + title;
                break;
            case 'twitter': serviceUrl = "http://twitter.com/home?status=" + title + "%20" + url;
                break;
            case 'digg': serviceUrl = "http://digg.com/submit?phase=2&amp;url=" + url + "&amp;title=" + title;
                break;
            case 'delicious': serviceUrl = "http://del.icio.us/post?url=" + url + "&amp;title=" + title;
                break;
            case 'stumbleupon': serviceUrl = "http://www.stumbleupon.com/submit?url=" + url + "&amp;title=" + title;
                break;
            case 'linkedin': serviceUrl = "http://www.linkedin.com/shareArticle?mini=true&url=" + url + "&title=" + title + "&ro=false&summary=&source=";
            //case 'linkedin': serviceUrl = "http://www.linkedin.com/shareArticle?mini=true&amp;url=" + url + "&amp;title=" + title + "&amp;ro=false&amp;summary=&amp;source=";
                break;
            case 'google': serviceUrl = "http://www.google.com/bookmarks/mark?op=edit&amp;bkmk=" + url + "&amp;title=" + title;
                break;
            case 'yahoo': serviceUrl = "http://bookmarks.yahoo.com/toolbar/savebm?opener=tb&amp;u=" + url + "&amp;t=" + title;
                break;
            case 'blogger': serviceUrl = "http://www.blogger.com/blog_this.pyra?u=" + url + "&amp;n=" + title;
                break;
            case 'myspace': serviceUrl = "http://www.myspace.com/Modules/PostTo/Pages/?u=" + url + "&amp;t=" + title;
                break;
        }
		if ( ! (service == 'email' )   ) {
			$(this).attr('target','_blank');
		}
		$(this).attr('href', serviceUrl);
		$(this).click();
        //window.open(serviceUrl);
        return false;
    });

});