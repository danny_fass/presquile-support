var v65 = {
	global : {
		mainMenuHover : function(){
			$(".mainMenu ul li:last-child").css("margin-right", "0");
			$(".mainMenu ul li ul li").hover(function(){
				$(this).parent().parent().children("a").toggleClass("hover");
			});
		},			
	},
	home : {
		initPhotoGallery : function(){
			if($("#slider").length){
				$("#slider").v65PhotoGallery({
					galleryHeight : 350, // This changes the height of the homepage photogallery
					galleryWidth : 950, // This changes the width of the homepage photogallery
					galleryId : "f3d4d014-bba0-e065-14b7-334053608ba6"	//	This is where you add the homepage photogallery id
				});
			}
		}
	},
	page : {
		initPhotoGallery : function(){
			if($("#pagePhotoGallery").length){
				$("#pagePhotoGallery").v65PhotoGallery({
					/*
						Uncomment the code below if you want to change how the photo gallery is displayed.

						galleryHeight : 420, // This value is translated to 420px and will change the photogallery height
						galleryWidth : 630, // This value is translated to 630px and will change the photogallery width
						pauseTime : 5000, // Adjust how long the image is displayed for. Value is in milliseconds
						animSpeed : 1000, // Adjust the transition speed between images. Value is in milliseconds
						controlNav : false, // hide the 1,2,3 navigation
						directionNav : false // hide the arrow navigation
					*/
				});
			}
		},
		initVintageInfoTabs : function(){
			if($("#vintage-info-tabgroup").length){
				
				// if this is running there must be js, so
				// show the tabs
				// and get rid of the h2 headings
				$("#vintage-info-tabs").show();
				$("h2.vintage-info-tab-content-heading").remove();
				
				// if there is teaser,
				if($(".v65-product-teaser").length){
					// make Reviews active by default
					$("#tab-Reviews").addClass('active');
					$("#tab-content-Reviews").addClass('active');
					
					// set up Reviews tab clicks
					$("#tab-Reviews a").click(function() {
						$(".vintage-info-tab").removeClass('active');
						$("#tab-Reviews").addClass('active');
						$(".vintage-info-tab-content").removeClass('active');
						$("#tab-content-Reviews").addClass('active');						
					});

				} else {
				// if no teaser, 

					// make Specifics active by default
					$("#tab-Specifics").addClass('active');
					$("#tab-content-Specifics").addClass('active');
										
					// remove Reviews
					$("#tab-Reviews").remove();
					$("#tab-content-Reviews").remove();
					
				}
				
				// set up Specifics tab click
				$("#tab-Specifics a").click(function() {
					$(".vintage-info-tab").removeClass('active');
					$("#tab-Specifics").addClass('active');
					$(".vintage-info-tab-content").removeClass('active');
					$("#tab-content-Specifics").addClass('active');						
				});				

				// if there is wineMakerNotes, set up More tab click
				// if none, remove More tab and tab content
				if($(".v65-wine-wineMakerNotes").length){
					$("#tab-More a").click(function() {
						$(".vintage-info-tab").removeClass('active');
						$("#tab-More").addClass('active');
						$(".vintage-info-tab-content").removeClass('active');
						$("#tab-content-More").addClass('active');						
					});
				} else {
					$("#tab-More").remove();
					$("#tab-content-More").remove();
				}
				
								
			}			
		},
		initCleanUnitDescription : function(){
			if($(".v65-product-addToCart-unitDescription").length){
				$(".v65-product-addToCart-unitDescription").each(function() {
					$(this).text( $(this).text().replace('/ ', '') );
				});
			}			
		},
		initCleanProductGroups : function(){

			// get rid of the .v65-group class to avoid vertical spacing it imposes
			$(".v65-productGroup").removeClass("v65-group");

			


			$(".v65-productGroup-product").each(function() {
				
				// provide variables to hold the price stored in the Security Message and/or Public Sale Ended Message
				var securityPriceText = ""; 
				var saleEndedPriceText = "";
				var recoveredPriceText = ""; 

				// if the product has a security message
				if (  $(this).find(".v65-product-addToCart-securityMessage").length  ){
					
					//retrieve the price stored in the Security Message
					//var securityPriceText; 
					var securityMessageEl = $(this).find(".v65-product-addToCart-securityMessage");
					securityPriceText = securityMessageEl.text();

					// customized contents of Security Message that will replace the price that was stored in there
					var clubHTML = 'Club Exclusive<span class="non-members"> | <a href="/index.cfm?method=memberLogin.showLogin" class="v65-modalLoginLink" v65js="modalLoginLink">Sign In</a> or <a href="/Wine-Club">Join the Club</a></span>';

					// insert the custom Security Message contents
					$(this).find(".v65-product-addToCart-securityMessage").html(clubHTML);
					
					// check for the presence of a logout link to determine if the user is logged in 
				    if ( $('.v65-logout').length ) {
				    	// if they are logged in, hide the login part of the Security Message 
				    	$(".non-members").css({
							"display":"none"
						});
				    }
/*
					var priceHTML = '<div class="v65-product-addToCart-priceWrapper"><div class="v65-product-addToCart-price" itemprop="price">' + priceText + '</div></div>';
					
					
					if (  ! ($(this).find(".v65-product-addToCart-outOfStockMessage").length ) ){
				    	$(this).find(".v65-product-addToCart-securityMessage").parents(".v65-product-addToCart").prepend(priceHTML);
			    	}
*/				    
				    
				}
				
				// if the product has a Public Sale Ended Message
				if (  $(this).find(".v65-product-addToCart-publicSaleEndedMessage").length  ){
					//retrieve the price stored in the Public Sale Ended Message
					var saleEndedEl = $(this).find(".v65-product-addToCart-publicSaleEndedMessage");
					saleEndedPriceText = saleEndedEl.text();
					
					// remove the Public Sale Ended Message
			    	saleEndedEl.css({
						"display":"none"
					});			
										
				}
				
				// if the product has no price already
				if (  ! ($(this).find(".v65-product-addToCart-price").length ) ){
					
					// try to get the price from the Security Message, otherwise try to get it from the Public Sale Ended Message
					if ( securityPriceText.length ) {
						recoveredPriceText = securityPriceText;
					} else if ( saleEndedPriceText.length ) {
						recoveredPriceText = saleEndedPriceText;
					}
					// if the price is recoverable from one of those message, stick it in form of the standard markup for the price element 
					if ( recoveredPriceText.length ) {
						var priceHTML = '<div class="v65-product-addToCart-priceWrapper"><div class="v65-product-addToCart-price" itemprop="price">' + recoveredPriceText + '</div></div>';
						$(this).find(".v65-product-addToCart").prepend(priceHTML);
					} 
					
				}
				
			});
			$(".v65-productGroup .v65-clear").remove();			
		},
		initProductDrilldown : function(){
			// if this is a product drill down page
			if($(".product-drilldown-page-htm").length){
				
				// HIGHLIGHT CURRENT PAGE IN LEFT NAV 
				var windowPathArray = window.location.pathname.split( '/' );

				// hide the tab group on Holiday Gifts detail pages
				if ( windowPathArray.length > 2 ) {
					if ( windowPathArray[2].toLowerCase() == "holiday-gifts" ) {
						$("#vintage-info-tabgroup").hide();						
					}
				}
				
				// second method of hiding tab group on Holiday Gifts detail pages
				// because marketing URLs aren't working, use hidden subtitle field
				if($(".v65-product-subtitle").length){
					if($(".v65-product-subtitle").text().toLowerCase() == "holiday gift" ){
						$("#vintage-info-tabgroup").hide();
						$(".v65-product-addToCart-unitDescription").css({
							"width":"auto",
							"margin-left":"-3px"
						});
					}					
				}
									

				// apply 'active' class to the parent li element
				// of the link to the current varietal 

				$("#leftNav-section-subpages > ul > li > a").each(function() {
					var strVarietalHref = $(this).attr('href');//.toLowerCase();
					
					var varietalLinkPathArray = strVarietalHref.split( '/' );
	
					if ( windowPathArray.length > 2 ) {
						if ( windowPathArray[2].toLowerCase() == varietalLinkPathArray[2].toLowerCase() ) {
							$(this).parents("#leftNav-section-subpages > ul > li").addClass('parentVarietalListItem').find('ul li a').each(function() {
								var strAppellationHref = $(this).attr('href');//.toLowerCase();
								var appellationLinkPathArray = strAppellationHref.split( '/' );
								if ( windowPathArray[3].toLowerCase() == appellationLinkPathArray[3].toLowerCase() ) {
									$(this).addClass('selectedAppellation');						
								}
							});						
						}
					}	
				});
				
				//CHANGE COLOR OF APPELLATION
				$('#wine-heading .v65-product-attributeValue:eq(1)').css('color', '#a2a2a2');

				// SET SECURITY MESSAGE FOR CLUB ONLY ITEMS
				if($(".v65-product-addToCart-securityMessage").length){
					var clubHTML = 'Club Exclusive<br /><a href="/index.cfm?method=memberLogin.showLogin" class="v65-modalLoginLink" v65js="modalLoginLink">Sign In</a> or <a href="/Wine-Club">Join the Club</a>'
					$(".v65-product-addToCart-securityMessage").html(clubHTML);
				}
			}
		},
		initVintageLinks : function(){
			if($(".vintage-link").length){
				var windowPathArray = window.location.pathname.split( '/' );
				
				// check if the last (year) part matches the text of vintage year menu items
				// and apply 'active' class if it matches
				$(".vintage-link").each(function() {
					var thisPathArray = $(this).attr('href').split( '/' );
					if ( thisPathArray[(thisPathArray.length - 1)].toLowerCase() == windowPathArray[(windowPathArray.length - 1)].toLowerCase() ) {
						$(this).addClass('selectedVintage');
					}
				});				
			}
			
			
		},
		initEventList : function(){
			
			// hide the events detail pages
			var windowPathArray = window.location.pathname.split( '/' );

			if ( windowPathArray.length > 2 ) {
				if (windowPathArray[2].toLowerCase() == "events") {
					$("#leftNav-menu ul li ul li ul").hide();
				}
			}
			
			// set up events vertical tabs menu
			if($(".v65-calendarList-Event").length){
				$(".v65-calendarList-Event").each(function() {
					var that = this;
					$(this).find('.v65-calendarList-Title a').click(function() {
						$(".v65-calendarList-Description").hide();
						$(that).find(".v65-calendarList-Description").show();
					});	
				});
				$(".v65-calendarList-Description").hide();
				$(".v65-calendarList-Description").first().show();
			}			
		},
		initTradeMedia : function(){
			// add linked thumbnails for pngs or jpgs
			$(".media-resource").each(function() {
				var linkEl = $(this).find(".media-resource-link");
				var linkURL = linkEl.attr('href');
				var linkType = "";
				var imageLinkHTML = '<a class="media-resource-thumbnail-link download-link" href="' + linkURL + '" target="_blank"><img src="' + linkURL + '"></a>'				
				
				if(linkURL.indexOf(".") != -1) {
					linkType = linkURL.substring(linkURL.lastIndexOf(".") + 1);
					if ( (linkType.toLowerCase() == "jpg" ) || ( linkType.toLowerCase() == "png" ) ) {
				    	$(this).append(imageLinkHTML);
				    }  
				} 				
			});		
		},
		initSpecialOffers : function(){
			// add linked thumbnails for pngs or jpgs
			$(".specialOffer-product .v65-product-photo a").click(function(e) {
				e.preventDefault();				
			});		
			$(".specialOffer-product .v65-product-title a").click(function(e) {
				e.preventDefault();			
			});				
		},
		initFlashFallbacks : function(){
			if(!(FlashDetect.installed)){
				
			}else{
				$(".flash-fallback").hide();     	
				
			}
		}				
	}
}

$(document).ready(function() {
	v65.global.mainMenuHover();
	v65.home.initPhotoGallery();
	v65.page.initPhotoGallery();
	v65.page.initVintageInfoTabs();
	v65.page.initCleanUnitDescription();
	v65.page.initCleanProductGroups();
	v65.page.initProductDrilldown();
	v65.page.initVintageLinks();
	v65.page.initEventList();
	v65.page.initTradeMedia();
	v65.page.initSpecialOffers();
	v65.page.initFlashFallbacks();
});

;(function($,undefined){
	$.fn.v65PhotoGallery = function(options){
		var defaults = {
			galleryId : $("#pagePhotoGallery").attr("v65jsphotogalleryid"),
			galleryHeight : $("#pagePhotoGallery").attr("v65jsphotogalleryheight"),
			galleryWidth : $("#pagePhotoGallery").attr("v65jsphotogallerywidth"),
			timestamp : "&timestamp="+ new Date().getTime(),
			effect:'fade', // Specify sets like: 'fold,fade,sliceDown'
			slices:15, // For slice animations
			animSpeed:500, // Slide transition speed
			pauseTime:5000, // How long each slide will show
			startSlide:0, // Set starting Slide (0 index)
			directionNav:true, // Next & Prev navigation
			directionNavHide:true, // Only show on hover
			controlNav:true // 1,2,3... navigation
		},
		gallery = $(this),
		settings = $.extend(defaults, options);
		gallery.html("").css({
			"height":settings.galleryHeight,
			"width":settings.galleryWidth,
			"overflow":"hidden"
		});
		$.ajax({
	    		type: "GET",
			url: "/index.cfm?method=pages.showPhotoGalleryXML&photogalleryid="+settings.galleryId+defaults.timestamp,
			dataType: "xml",
			success: function(xml) {
				var images = "";
				$(xml).find('img').each(function() {
					var location = '/assets/images/photogallery/images/large/',
						photo = $(this).attr('src'),
						caption = $(this).attr('caption'),
						url = $(this).attr('link');
					if (url === undefined) {
						images += '<img src="'+location+photo+'" title="'+caption+'"/>';
					} else{
						images += '<a href="'+url+'"><img src="'+location+photo+'" title="'+caption+'"/></a>';
					}
				});
				gallery.append(images);
			},
			complete: function(){
	   			gallery.nivoSlider({
					effect:settings.effect, // Specify sets like: 'fold,fade,sliceDown'
					slices:settings.slices, // For slice animations
					animSpeed:settings.animSpeed, // Slide transition speed
					pauseTime:settings.pauseTime, // How long each slide will show
					startSlide:settings.startSlide, // Set starting Slide (0 index)
					directionNav:settings.directionNav, // Next & Prev navigation
					directionNavHide:settings.directionNavHide, // Only show on hover
					controlNav:settings.controlNav // 1,2,3... navigation
				});
	   		}
	   	});
	}
})(jQuery);